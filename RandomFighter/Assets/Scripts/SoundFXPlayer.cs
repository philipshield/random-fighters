﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundFXPlayer : MonoBehaviour
{
    public static SoundFXPlayer instance;
    public List<GameObject> Sounds;

	void Awake()
    {

        //Singleton
        if (instance == null)
            instance = this as SoundFXPlayer;
        else
            Destroy(this.gameObject);
    }

    public void PlaySound(string soundname, Vector3 pos)
    {
		
		for(int i = 0; i < Sounds.Count; i++)
		{
			if(soundname.Equals(Sounds[i].name))
				Instantiate(Sounds[i], pos, Quaternion.identity);
		}
    }
}
