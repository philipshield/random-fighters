﻿using UnityEngine;
using System.Collections;
using System;

public class XController : MonoBehaviour
{
    public int _playerId;
    public event Action<ButtonData> OnButtonPress;
    public event Action<ButtonData> OnButtonRelease;
    public event Action<ButtonData> OnButtonHold;
    public event Action<JoystickData> OnJoystick;

    public float deadzoneRadius;
    private JoystickData _joystick;

    public XController(int id)
    {
        _joystick = new JoystickData(Vector2.zero, id);
    }

    void Update()
    {
        checkAxes();
        checkPresses();
        checkReleases();
    }

    private string buttonCode(XButton button)
    {
        if (_playerId > 4 || _playerId <= 0) throw new NotImplementedException();
        return button.ToString() + _playerId.ToString();
    }
    private void firePress(ButtonData data)
    {
        if (OnButtonPress != null)
            OnButtonPress.Invoke(data);
    }
    private void fireRelease(ButtonData data)
    {
        if (OnButtonRelease != null)
            OnButtonRelease.Invoke(data);
    }
    private void fireHold(ButtonData data)
    {
        if (OnButtonHold != null)
            OnButtonHold.Invoke(data);
    }


    private void checkAxes()
    {
        _joystick.PlayerId = _playerId; //TODO: remove later
        float horizontal = Input.GetAxis("Horizontal" + _playerId.ToString());
        float vertical = -Input.GetAxis("Vertical" + _playerId.ToString());

        Vector2 force = new Vector2(horizontal, vertical);

        if (force.magnitude < deadzoneRadius)
            force = Vector2.zero;

        _joystick.setForce(force);

        if (OnJoystick != null)
        {
            OnJoystick.Invoke(_joystick);
        }
    }

    private void checkPresses()
    {
        string s = buttonCode(XButton.A);
        if (Input.GetButtonDown(s))
        {
            firePress(new ButtonData(XButton.A, _playerId));
        }
        if (Input.GetButtonDown(buttonCode(XButton.B)))
        {
            firePress(new ButtonData(XButton.B, _playerId));
        }
        if (Input.GetButtonDown(buttonCode(XButton.X)))
        {
            firePress(new ButtonData(XButton.X, _playerId));
        }
        if (Input.GetButtonDown(buttonCode(XButton.Y)))
        {
            firePress(new ButtonData(XButton.Y, _playerId));
        }
        if (Input.GetButtonDown(buttonCode(XButton.Start)))
        {
            firePress(new ButtonData(XButton.Y, _playerId));
        }
    }

    private void checkReleases()
    {
        if(Input.GetButtonUp(buttonCode(XButton.A)))
        {
            fireRelease(new ButtonData(XButton.A, _playerId));
        }
        if (Input.GetButtonUp(buttonCode(XButton.B)))
        {
            fireRelease(new ButtonData(XButton.B, _playerId));
        }
        if (Input.GetButtonUp(buttonCode(XButton.X)))
        {
            fireRelease(new ButtonData(XButton.X, _playerId));
        }
        if (Input.GetButtonUp(buttonCode(XButton.Y)))
        {
            fireRelease(new ButtonData(XButton.Y, _playerId));
        }
        if (Input.GetButtonUp(buttonCode(XButton.Start)))
        {
            fireRelease(new ButtonData(XButton.Start, _playerId));
        }
    }


    /* CURRENT STATE */

    public bool GetButtonHold(XButton button) { return false; }
    public bool GetButtonDown(XButton button) { return Input.GetButtonDown(buttonCode(button)); }
    public bool GetButtonRelease(XButton button) { return Input.GetButtonUp(buttonCode(button)); }
    public JoystickData GetJoystickLeft() { return _joystick; }

}
