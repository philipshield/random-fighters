﻿using UnityEngine;
using System.Collections;

public struct ButtonData 
{
    public int PlayerId;
    public float HoldTime;
    public XButton Button;

    public ButtonData(XButton button, int id)
    {
        Button = button;
        PlayerId = id;
        HoldTime = 0;
    }
}
