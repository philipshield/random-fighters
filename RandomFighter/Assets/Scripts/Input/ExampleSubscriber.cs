﻿using UnityEngine;
using System.Collections;

public class ExampleSubscriber : MonoBehaviour {

    public XController InputControl1;
    public XController InputControl2;

	// Use this for initialization
	void Start () 
    {
        InputControl1.OnButtonPress += OnButtonPress;
        InputControl1.OnButtonRelease += OnButtonRelease;

        InputControl2.OnButtonPress += OnButtonPress;
        InputControl2.OnButtonRelease += OnButtonRelease;

        InputControl1.OnJoystick += OnJoystick;
        InputControl2.OnJoystick += OnJoystick;
	}

    void OnButtonRelease(ButtonData data)
    {
//        Debug.Log("Player " + data.PlayerId.ToString() + " Key Release: " + data.Button.ToString());
    }

    void OnButtonPress(ButtonData data)
    {
//        Debug.Log("Player " + data.PlayerId.ToString() + " Key Pressed: " + data.Button.ToString());
    }

    void OnJoystick(JoystickData data)
    {
//        Debug.Log("Player " + data.PlayerId.ToString() + " Joystick: " + data.Force.ToString());
    }
	
	// Update is called once per frame
	void Update () 
    {
        //Debug.Log(InputControl1.GetJoystickLeft().Force.ToString());
	
	}
}
