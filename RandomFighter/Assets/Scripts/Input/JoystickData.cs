﻿using UnityEngine;
using System.Collections;

public struct JoystickData
{
    public Vector2 Force;
    public int PlayerId;

    public JoystickData(Vector2 force, int id)
    {
        Force = force;
        PlayerId = id;
    }

    public void setForce(float horizontal, float vertical)
    {
        setForce(new Vector2(horizontal, vertical));
    }
    public void setForce(Vector2 force)
    {
        Force = force;
    }
}
