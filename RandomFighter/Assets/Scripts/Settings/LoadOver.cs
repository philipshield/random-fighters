﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadOver : MonoBehaviour 
{
    public List<GameObject> players = new List<GameObject>();
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void OnLevelWasLoaded(int level)
    {
        foreach (var p in players)
        {
            p.SetActive(true);
        }
    }
}
