﻿using UnityEngine;
using System.Collections;

public class MatchSettings : MonoBehaviour {
	int _players = 0;
	bool _spawned = false;

	void Awake()
	{
		DontDestroyOnLoad(this.gameObject);
	}
	void Update()
	{
		if(Input.GetButtonUp("Fire1"))
		{
			if(Application.loadedLevelName == ("MovementTest") && !_spawned)
			{
				_spawned = true;
				GameObject.Find("GameManager").GetComponent<GameManager>().Initialize(_players);
			}
			else
				AddPlayer();
		}
		if(Application.loadedLevelName != ("MovementTest"))
			if(Input.GetButtonUp("Fire2"))
				Application.LoadLevel("MovementTest");
	}
	void AddPlayer()
	{
		if(_players < 2)
			_players++;
	}
	void Start()
	{
		if(Application.loadedLevelName != ("MovementTest"))
		{
			_spawned = false;
			_players = 0;
		}
	}
}
