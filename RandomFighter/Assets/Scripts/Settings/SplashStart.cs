﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SplashStart : MonoBehaviour
{
    public GameObject prefab_player;
    public Transform[] spawnpoints;
    public bool[] connected;

    public AbilityList abilityList;

    private const int nPlayers = 4;
    private int nConnected;

    void Start()
    {
//        Debug.Log("Starting Loader");
        if (spawnpoints.Length != nPlayers) throw new NotImplementedException();
        if (prefab_player == null) throw new NotImplementedException();
        connected = new bool[4];
        nConnected = 0;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0, id = 1; i < nPlayers; ++i, ++id)
        {
            if (Input.GetButtonDown("A" + (id).ToString()) && !connected[i])
            {
                //Connect a new player!
//                Debug.Log("NEW Player! :D (" + (id).ToString() + ")");
                
                nConnected++;
                connected[i] = true;
                Menu.instance.ConnectedPlayer(i);
				SoundFXPlayer.instance.PlaySound("FightFX - pressA", Camera.main.transform.position);
            }

            if (Input.GetButtonDown("B" + id.ToString()) && connected[i])
            {
//                Debug.Log("DISCONNECT player " + id.ToString());
                Menu.instance.DisconnectedPlayer(i);
                nConnected--;
                connected[i] = false;
				SoundFXPlayer.instance.PlaySound("FightFX - pressB", Camera.main.transform.position);
            }

            if (Input.GetButtonDown("Start" + id.ToString()))
            {
				SoundFXPlayer.instance.PlaySound("FightFX - pressSTART", Camera.main.transform.position);
                if (nConnected < 1)
                {
//                    Debug.Log("Too few players");
                }
                else
                {
//                    Debug.Log("STARTING GAME!");
                    doStart();
                }
            }
        }
    }

    T CopyComponent<T>(T original, GameObject destination) where T : Component
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy as T;
    }

    void doStart()
    {
        GameObject players = GameObject.Find("Players");
        LoadOver load = players.GetComponent<LoadOver>();

        //create players
        for(int i = 0, id = 1; i < nPlayers; ++i, ++id)
        {
            if (!connected[i]) continue;

            GameObject player = Instantiate(prefab_player, 
                                            spawnpoints[i].position, 
                                            Quaternion.identity) as GameObject;

            //assign tag
            player.tag = "Player_"+id.ToString();

            //assign charatermove id
            player.GetComponent<CharacterMove>().SetID(id);

            //assign controller
            player.GetComponent<XController>()._playerId = id;

            load.players.Add(player);


            List<Ability> ab = abilityList.GetRandomAbilities(4);
            int j = 0;
            XButton[] buttons = new XButton[]{XButton.A, XButton.B, XButton.X, XButton.Y};
            foreach(Ability a in ab)
            {
                Ability aaa = CopyComponent(a, player);
                aaa.Button = buttons[j];
                ++j;
            }

            player.SetActive(false);
            
            player.transform.parent = players.transform;

        }
        
        //THÄ FIGHT LOAD
        Application.LoadLevel("MovementTest");
    }

}
