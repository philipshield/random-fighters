﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public GameObject player;
	public GameObject[] spawnPoints;
	public StarHolder[] playerPoints;
	
	int[] Points = new int[4]{0,0,0,0};
	int _victoryPoints = 3;
	
	
	void Start()
	{
		Menu.instance.SetStars(playerPoints);
	}
	// Use this for initialization
	public void Initialize(int players) {
		for(int i = 0; i < players; i++)
		{
			GameObject go = Instantiate(player, spawnPoints[i].transform.position, Quaternion.identity) as GameObject;
			go.name = "Player" + i.ToString();
			go.GetComponentInChildren<CharacterMove>().SetID(i+1);
		}
		
	}
	public void UpdatePlayerPoints(int player, int hit)
	{
//		Debug.Log(hit);
		if(hit > 0)
		{
			int temp = hit -1;
			Points[temp]++;
			Menu.instance.DisplayMessage(temp, Points[temp]);
			if(Points[temp] == _victoryPoints)
			{
				Menu.instance.TogglePaus();
				StartCoroutine("Restart");
			}
		}
		else
		{

			int temp = player -1;
			if(Points[temp] > 0)
			{
				Points[temp]--;
				Menu.instance.DisplayMessage(temp, Points[temp]);
				if(Points[temp] <= 0)
				{
					Menu.instance.DisplayMessage(temp, Points[temp]);
				}
			}
		}
			
	}
	IEnumerator Restart()
	{
		yield return new WaitForSeconds(3.0f);
		Menu.instance.Restart();
	}
	public Vector3 SpawnPosition()
	{
		return spawnPoints[Random.Range(0,4)].transform.position;
	}
}
