﻿using UnityEngine;
using System.Collections;
using System;

public class Knockout : MonoBehaviour
{
    [SerializeField]
    private float m_maxHp = 100;
    [SerializeField]
    private float m_minHp = -50;
    [SerializeField, Tooltip("Maximum health after knockout")]
    private float m_minRecoverHealth = 30;
    [SerializeField, Tooltip("Minimum health after recover")]
    private float m_maxKnockoutHealth = -30;
    [SerializeField, Tooltip("HP per second")]
    private float m_recoverRate = 15;
    [SerializeField]
    private Transform m_ninjaTransform = null;

    [SerializeField]
    private float m_hp;

    public event Action OnKnockedOut;
    public event Action OnRecover;

    public bool IsKnockedOut { get{ return m_hp < 0; } }

    public float HpFactor
    {
        get { return m_hp / m_maxHp; }
    }

    public void Restore()
    {
        m_hp = m_maxHp;
    }

    public void DealDamage(float damage)
    {
        bool knockout = m_hp > 0 && m_hp - damage < 0;

        m_hp -= damage;
        if (m_hp < m_minHp)
            m_hp = m_minHp;

        if (knockout)
        {
            if (m_hp > m_maxKnockoutHealth)
                m_hp = m_maxKnockoutHealth;

            if(OnKnockedOut != null)
                OnKnockedOut();
        }
    }

    void Start()
    {
        m_hp = m_maxHp;
    }

    void Update()
    {
        float oldHp = m_hp;
        m_hp = Mathf.Min(m_maxHp, m_hp + m_recoverRate * Time.deltaTime);

        if(m_ninjaTransform != null)
        {
            float spin = Mathf.Max(0, -m_hp) * 30;
            m_ninjaTransform.localRotation = Quaternion.Euler(Mathf.Sin(spin / 80.0f) * 20, spin, Mathf.Sin(spin / 70.0f) * 15);
        }

        if (oldHp < 0 && m_hp > 0)
        {
            if (m_hp < m_minRecoverHealth)
                m_hp = m_minRecoverHealth;

            if(OnRecover != null)
                OnRecover();
        }
    }
}
