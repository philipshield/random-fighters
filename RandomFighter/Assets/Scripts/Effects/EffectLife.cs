﻿using UnityEngine;
using System.Collections;

public class EffectLife : MonoBehaviour {
	float _lifeTime = 0.5f;
	// Use this for initialization
	void Start () {
		StartCoroutine("Life");
	}
	IEnumerator Life()
	{
		yield return new WaitForSeconds(_lifeTime);
		Destroy(this.gameObject);
	}
}
