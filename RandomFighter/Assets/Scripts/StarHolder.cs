﻿using UnityEngine;
using System.Collections;

public class StarHolder : MonoBehaviour {
	public GameObject[] Stars = new GameObject[3];
	
	public GameObject[] GetStars
	{
		get{return Stars;}
	}
}
