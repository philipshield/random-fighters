﻿using UnityEngine;
using System.Collections;

public class LastHit : MonoBehaviour {
	int _hitBy = 0;
	bool _recentlyHit = false;
	public int WasHitBy()
	{
		return _hitBy;
	}
	public void HitBy(int i)
	{
		if(i > 0)
		{
			StartCoroutine("RecentlyHit");
			_hitBy = i;
		}
		else
		{
			if(!_recentlyHit)
			{
				_hitBy = 0;
			}
		}
	}
	IEnumerator RecentlyHit()
	{
		if(_recentlyHit)
			yield break;
		else
		{
			_recentlyHit = true;
			yield return new WaitForSeconds(5.0f);
			_recentlyHit = false;
		}
	}
}
