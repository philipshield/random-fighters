﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimController : MonoBehaviour {
	
	public AudioClip sound;
	
	Animation anim;
	
	float _actionAnimSpeed = 1.0f;
	float _actionAnimPos = 0.0f;

    string _currentBasicAnim = "idle";
    string _currentActionAnim = null;

	void Start()
	{
		anim = GetComponentInChildren<Animation>();
		anim.Play(_currentBasicAnim);
	}

    public void SetBasicAnimation(string i)
    {
        if (i != _currentBasicAnim)
        {
            _currentBasicAnim = i;

            if (_currentActionAnim == null)
            {
                anim.CrossFade(_currentBasicAnim, 0.1f, PlayMode.StopAll);
            }
        }
    }

	public void StartActionAnimation(string i)
	{
        _currentActionAnim = i;
        anim.Play(_currentActionAnim, PlayMode.StopAll);

        _actionAnimSpeed = 1;
        anim[_currentActionAnim].speed = _actionAnimSpeed;
        anim[_currentActionAnim].time = 0;
	}

    void Update()
    {
        if (_currentActionAnim != null)
        { 
            AnimationState state = anim[_currentActionAnim];

            if(!state.enabled)
            {
                StopActionAnimation();
            }
        }
    }

	public void StopActionAnimation()
	{
		if(_currentActionAnim != null)
        {
            anim.CrossFade(_currentBasicAnim, 0.1f, PlayMode.StopAll);
            anim[_currentBasicAnim].speed = 1.0f;
            _currentActionAnim = null;
		}
		
	}
	public float ActionAnimationSpeed
	{
		get
		{
			return _actionAnimSpeed;
		}
		set
		{
			_actionAnimSpeed = value;
            if (_currentActionAnim != null)
                anim[_currentActionAnim].speed = ActionAnimationSpeed;
		}
	}
	public float ActionAnimationPos
	{
		get
		{
            if(_currentActionAnim != null)
            {
                AnimationState state = anim[_currentActionAnim];
                return state.time / state.length;
            }
			return 0;
		}
		set
        {
            if (_currentActionAnim != null)
            {
                AnimationState state = anim[_currentActionAnim];
                state.time = value * state.length;
            }
		}
	}
}