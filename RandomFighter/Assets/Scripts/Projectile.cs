﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Projectile : MonoBehaviour
{
    private static string[] m_playerTags = { "Player_1", "Player_2", "Player_3", "Player_4" };

    [SerializeField]
    private float m_lifeTime;

    public float LifeTime
    {
        get { return m_lifeTime; }
        set { m_lifeTime = value; }
    }

    public GameObject Owner { get; set; }
    
    public GameObject Effect {get; set;}

    public Transform Anchor { get; set; }

    private Dictionary<string, float> m_hits = new Dictionary<string,float>();

    public event Action<GameObject, float, Projectile> OnHitPlayer;
    public event Action<Projectile, float, Projectile> OnHitProjectile;
    public event Action<GameObject, float, Projectile> OnHitOther;
    public event Action<Projectile> OnLifetimeExceeded;

    private Vector2 m_position;
    private Vector2 m_velocity;

    public void SetPosition(Vector2 position)
    {
        if (Anchor != null)
        {
            m_position = position;
        }
        else
        {
            rigidbody2D.position = position;
        }
    }

    public void SetVelocity(Vector2 velocity)
    {
        if (Anchor != null)
        {
            m_velocity = velocity;
            m_velocity.x = Mathf.Abs(m_velocity.x);
        }
        else
        {
            rigidbody2D.velocity = velocity;
        }
    }

    void Update()
    {
        LifeTime -= Time.deltaTime;
        if (LifeTime < 0)
        {
            if (OnLifetimeExceeded != null)
                OnLifetimeExceeded(this);

            Destroy(this.gameObject);
        }
    }

    void FixedUpdate()
    {
        if (Anchor != null)
        {
            m_position -= m_velocity * Time.fixedDeltaTime;

            Vector3 pos = Anchor.position + Anchor.rotation * (Vector3)m_position;

            rigidbody2D.velocity = ((Vector2)pos - rigidbody2D.position) / Time.fixedDeltaTime;
        }
    }

    public void SetRadius(float radius)
    {
        this.transform.localScale = new Vector3(radius, radius, radius);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Projectile other = collider.gameObject.GetComponent<Projectile>();
        if (collider.gameObject.tag == tag)
            return;

        float time;
        if (!m_hits.TryGetValue(collider.tag, out time))
            time = -1.0f;
        float timeSinceLastHit = time < 0 ? -1.0f : Time.time - time;

        if (other != null)
        {
            //Hit other projectile
            if (OnHitProjectile != null)
            {
                OnHitProjectile(other, timeSinceLastHit, this);
				SpawnEffect(other.gameObject, transform.position);
			}
		}
        else
        {
            //Hit player
            if (m_playerTags.Contains(collider.tag))
            {
                if (OnHitPlayer != null)
                {
                    OnHitPlayer(collider.gameObject, timeSinceLastHit, this);
					SpawnEffect(collider.gameObject, transform.position);
					collider.GetComponent<LastHit>().HitBy(Owner.GetComponent<CharacterMove>().GetID());
				}
			}
            else //Hit other
            {
                if (OnHitOther != null)
                {
                    OnHitOther(collider.gameObject, timeSinceLastHit, this);
                    SpawnEffect(collider.gameObject, transform.position);
                } 
            }
        }
		SoundFXPlayer.instance.PlaySound("FightFX - fightImpact01", new Vector3(m_position.x, m_position.y, Camera.main.transform.position.x));
        m_hits[collider.tag] = Time.time;
    }
	void SpawnEffect(GameObject hit, Vector3 pos)
	{
		if(Effect != null)
		{
			GameObject e = Instantiate(Effect, pos, Quaternion.identity) as GameObject;
			e.transform.LookAt(hit.transform.position);
		}
	}
}
