﻿using UnityEngine;
using System.Collections;

public class CharacterMove : MonoBehaviour
{
    public LayerMask mask;
	public GameObject DashEffect;
	public GameObject SpawnEffect;
	public GameObject LandEffect;
	public GameObject Feet;
	public Renderer mat;
	public Texture2D[] textures;
    int ID = 1;

    public float _downForce = 30.0f;
    public float _moveForce = 1.0f;
    public float _maxSpeed = 5.0f;
    public float _airMoveForce = 0.7f;
    public float _speedIncrease = 1.0f;
    public float _jumpForce = 2.0f;
    public float _friction = 1.0f;
    public float _airMoveFriction = 1.0f;
    bool _isRunning = false;
    bool _isWalking = false;
    bool _drop = false;
    bool _fallingfast = true;
    GameManager gameMan;
    
   // bool _facingRight = false;

    public enum CharacterDirection
    { 
        Left,
        Right
    }

    public CharacterDirection Direction { get; private set; }

    GroundSensor groundSens;
    XController controller;
    Knockout _knockout;
	AnimController anim;
	LastHit hitBy;

    void Start()
    {
        GroundCheck();
        groundSens = GetComponentInChildren<GroundSensor>();
        _knockout = GetComponent<Knockout>();
		anim = GetComponent<AnimController>();
        controller = GetComponent<XController>();
        Direction = CharacterDirection.Left;
        hitBy = GetComponent<LastHit>();
        gameMan = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    void FixedUpdate()
    {
        GroundCheck();
        
        if (rigidbody2D.velocity.magnitude < 0.25f)
        {
			anim.SetBasicAnimation("idle");
            //rigidbody2D.drag = 0.0f;
            _isWalking = false;
            _isRunning = false;
            _speedIncrease = 1.0f;
        }
        else
        {
			anim.SetBasicAnimation("run");
            //rigidbody2D.drag = 15.0f;
        }
        if (groundSens.IsOnGround)
        {
            if (!_isWalking && Input.GetButtonDown("Horizontal" + ID.ToString()))
            {
                _isWalking = true;
            }
            if (_isWalking && Input.GetButtonDown("Horizontal" + ID.ToString()) && Time.deltaTime < 0.75f)
            {
                _isRunning = true;
				Instantiate(DashEffect, Feet.transform.position, Quaternion.identity);
            }
            if (_isRunning)
                _speedIncrease = 2.0f;
                
			if(hitBy != null)
				if(hitBy.WasHitBy() != 0)
					hitBy.HitBy(0);
					
			if(_fallingfast)
			{
				_fallingfast = false;
				SoundFXPlayer.instance.PlaySound("FightFX - moveLand02", new Vector3(Feet.transform.position.x, Feet.transform.position.y, Camera.main.transform.position.x));
				GameObject smokeLand = Instantiate(LandEffect, Feet.transform.position, Quaternion.identity) as GameObject;
				smokeLand.name = "Smoke";
			}

        }
        else if (!groundSens.IsOnGround || _isWalking)
        {
            _speedIncrease = 1.0f;
        }
        if (!_knockout.IsKnockedOut)
        {
            //TODO: sprint
            float horizontalAxis = controller.GetJoystickLeft().Force.x;

            if (horizontalAxis < -0.1f)
            {
                Direction = CharacterDirection.Left;
            }
            else if(horizontalAxis > 0.1f)
            {
                Direction = CharacterDirection.Right;
            }

            rigidbody2D.AddForce((groundSens.IsOnGround ? _moveForce : _airMoveForce) * (horizontalAxis * _maxSpeed - rigidbody2D.velocity.x) * Mathf.Abs(horizontalAxis) * _knockout.HpFactor * Vector2.right);
        }

        float hp = Mathf.Max(0, _knockout.HpFactor);
        Vector2 frictionForce = -(groundSens.IsOnGround ? _friction / (Mathf.Abs(rigidbody2D.velocity.x) + 0.5f) : _airMoveFriction * hp) * rigidbody2D.velocity;
        if (groundSens.IsOnGround)
            frictionForce.y = 0;
        rigidbody2D.AddForce(frictionForce);

		/*if (controller.GetJoystickLeft().Force.x > 0 && !_facingRight)
		{
			Flip();
		}
		if (controller.GetJoystickLeft().Force.x < 0 && _facingRight)
		{
			Flip();
		}*/
        Vector3 angles = transform.rotation.eulerAngles;
        float target = Direction == CharacterDirection.Left ? 0 : 180;
        angles.y += 8.0f * Time.fixedDeltaTime * (target - angles.y);
        transform.rotation = Quaternion.Euler(angles);
       
        if (!groundSens.IsOnGround)
        {
            float y = controller.GetJoystickLeft().Force.y;

            if (y > 0)
                y *= 0.5f;

            rigidbody2D.AddForce(Vector2.up * y * _downForce * hp);
        }
        

        if (controller.GetJoystickLeft().Force.y < -0.8f)
        {
            if (!_drop)
                Drop();
        }
        if (rigidbody2D.velocity.y > 0)
        {
            this.gameObject.layer = LayerMask.NameToLayer("PlayerDown");
        }
        if(groundSens.IsOnGround == false)
        {
			if(rigidbody2D.velocity.y > 0.25f)
			{
				anim.SetBasicAnimation("jumpAscend");
			}
			if(rigidbody2D.velocity.y > -0.1f && rigidbody2D.velocity.y < 0.1f)
			{
                anim.SetBasicAnimation("jumpToDescend");
			}
			if(rigidbody2D.velocity.y < 0.1f)
			{
                anim.SetBasicAnimation("jumpDescend");
			}
			if(rigidbody2D.velocity.y < -20.0f)
			{
				_fallingfast = true;
			}
        }
    }
   /* void Flip()
    {
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		_facingRight = !_facingRight;
    }*/
    void Drop()
    {
    	anim.StartActionAnimation("jump");
        _drop = true;
        this.gameObject.layer = LayerMask.NameToLayer("PlayerDown");
        GroundCheck();
    }
    void GroundCheck()
    {

        RaycastHit2D hit = Physics2D.Raycast(Feet.transform.position, -Vector2.up, 0.15f, mask);
        if (hit.collider != null)
        {
            if (_drop)
            {
                if (hit.collider.name != "Platform" && hit.collider != this.collider)
                {
                    this.gameObject.layer = LayerMask.NameToLayer("Player" + ID.ToString());
					
                    _drop = false;

                }
            }
            else
            {
                this.gameObject.layer = LayerMask.NameToLayer("Player" + ID.ToString());

            }
            if(hit.transform.tag == "DeathZone")
            	Respawn();
        }
    }
    void Jump()
    {
        this.gameObject.layer = LayerMask.NameToLayer("PlayerDown");
        rigidbody2D.AddForce(Vector2.up * _jumpForce, ForceMode2D.Impulse);
    }
    public void SetID(int id)
    {
        ID = id;
        this.gameObject.layer = LayerMask.NameToLayer("Player" + ID.ToString());
        mat.material.mainTexture = textures[ID-1];
    }
	public int GetID()
	{
		return ID;
	}
	void Respawn()
	{
		StartCoroutine("SpawnLock");
		mat.enabled = false;
		rigidbody2D.velocity = new Vector2(0,0);
		gameMan.UpdatePlayerPoints(ID, GetComponent<LastHit>().WasHitBy());
		transform.position = gameMan.SpawnPosition();
		rigidbody2D.isKinematic = true;
		hitBy.HitBy(0);
        _knockout.Restore();
        
	}
	IEnumerator SpawnLock()
	{
		
		yield return new WaitForSeconds(2.0f);
		mat.enabled = true;
		GameObject Ef = Instantiate (SpawnEffect, Feet.transform.position, Quaternion.identity)as GameObject;
		rigidbody2D.isKinematic = false;
	}
}
