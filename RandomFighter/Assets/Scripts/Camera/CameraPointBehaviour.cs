﻿using UnityEngine;
using System.Collections;

public class CameraPointBehaviour : MonoBehaviour 
{
    public Transform Position;
    public Transform CameraDesiredPosition;
    public bool UseDesiredLookat;
    public Transform CameraDesiredLookat;
    public float radius;

    void Start()
    {
    }
}
