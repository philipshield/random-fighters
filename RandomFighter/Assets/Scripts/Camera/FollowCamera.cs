﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowCamera : MonoBehaviour 
{
    private List<CameraPointBehaviour> cameraPoints = new List<CameraPointBehaviour>();
    private List<Transform> playerTransforms = new List<Transform>();
    public Vector3 Outwards;
    public Vector3 Offset;
    public float MinOutzoom;
    public float MaxOutzoom;
    public float MaxForce;
    public float MaxSpeed;

    public float Straightness;
    public float DefaultLookatSpeed;
    public float IntoPointLookatSpeed;

    private Vector3 velocity;
    private Vector3 acceleration;

	void Start () 
    {
        Outwards.Normalize();

        foreach (var point in GameObject.FindObjectsOfType<CameraPointBehaviour>())
        {
            cameraPoints.Add(point);
        }

        LoadOver loadover = GameObject.FindObjectOfType<LoadOver>();
        foreach(var player in loadover.players)
        {
            playerTransforms.Add(player.transform);
        }
	}
	
	void Update ()
    {
        //update velocity
        velocity += acceleration;
        velocity = Vector3.ClampMagnitude(velocity, MaxSpeed);
        acceleration *= 0;
        transform.position += velocity * Time.deltaTime;

        //find suitable target (between players)
        Vector3 target = playerTransforms[0].position;
        for(int i = 1; i < playerTransforms.Count; ++i)
            target += playerTransforms[i].position;
        target /= playerTransforms.Count;

        //close to camera behaviour point?
        foreach (var point in cameraPoints)
        {
            float avgdist2 = 0;
            foreach(Transform playert in playerTransforms)
            {
                avgdist2 += (point.Position.position - playert.position).sqrMagnitude;
            }
            avgdist2 /= playerTransforms.Count;

            if(avgdist2 < Mathf.Pow(point.radius, 2))
            {
                //Debug.Log("point within radius!");
                applyforce(seek(point.CameraDesiredPosition.position, MaxForce));
                Vector3 newtarget = point.UseDesiredLookat ? point.CameraDesiredLookat.position : target;
                Vector3 lookat = Vector3.Lerp(target, newtarget, Time.deltaTime*0.4f);
                //Vector3 lookat = newtarget;
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookat - transform.position), Time.deltaTime*IntoPointLookatSpeed);
                return; //(pick only first point)
            }
        }

        //seek towards average, but zoom out from the stage
        Vector3 desiredposition = target + Outwards * MinOutzoom + Offset;
        applyforce(seek(desiredposition, MaxForce));

        this.transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(target - transform.position), Time.deltaTime*DefaultLookatSpeed);
		
		if(Input.GetKeyUp(KeyCode.Escape))
			Menu.instance.TogglePaus();
	}

    private Vector3 seek(Vector3 target, float maxforce)
    {
        //desired
        Vector3 desired = (target - transform.position);
        float d = desired.magnitude;
        if (d == 0) return Vector3.zero;

        //steering
        Vector3 steer = Vector3.ClampMagnitude(desired - velocity, maxforce);

        return steer;
    }

    private void applyforce(Vector3 force)
    {
        acceleration += force;
    }
}
