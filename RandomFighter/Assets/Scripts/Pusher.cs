﻿using UnityEngine;
using System.Collections;
using System;

//Abilities should add forces to the rigidbody through this interface instead of directly
public class Pusher : MonoBehaviour 
{
    public void ApplyImpulse(Vector2 impulse)
    {
        //Handle for impulses

        rigidbody2D.AddForce(impulse, ForceMode2D.Impulse);
    }
    public void ApplyForce(Vector2 force)
    {
        //Handle for forces

        rigidbody2D.AddForce(force);
    }

    public void SetVelocity(Vector2 velocity)
    {
        rigidbody2D.velocity = velocity;
    }
}
