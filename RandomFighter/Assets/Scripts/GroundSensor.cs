﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GroundSensor : MonoBehaviour
{
    private int m_collisionCount = 0;
    public bool IsOnGround { get { return m_collisionCount != 0; } }

    public event Action OnHitGround;
    public event Action OnLeaveGround;

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag != "Ground")
            return;

        ++m_collisionCount;

        if (m_collisionCount == 1 && OnHitGround != null)
        {
            OnHitGround();
        }
    }

    void OnTriggerExit2D(Collider2D other) 
    {
        if (other.tag != "Ground")
            return;

        --m_collisionCount;

        if (m_collisionCount == 1 && OnLeaveGround != null)
        {
            OnLeaveGround();
        }
    }
}
