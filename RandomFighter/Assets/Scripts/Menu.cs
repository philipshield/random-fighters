﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	public GameObject playermanager;
	public static Menu instance;
	
	StarHolder[] playerPoints;
	GameObject[,] playerStars = new GameObject[4,3];
	
	string text = "PAUSED";
	bool _isPaused = false;
	string[] PlayerPoints = new string[4]{"Player 1:","Player 2:","Player 3:","Player 4:"};
	int[] points = new int[4]{0,0,0,0};
	
	bool[] connected = new bool[4]{false,false,false,false};
	
	void Awake()
	{
		DontDestroyOnLoad(this.gameObject);
		if(instance == null)
			instance = this as Menu;
		else
			Destroy (this.gameObject);
		
	}
	
	public void SetStars(StarHolder[] stars)
	{
//		Debug.Log(stars[0].gameObject.name);
		playerPoints = stars;
		for(int i = 0; i < playerPoints.Length; i++)
		{
			for(int t = 0; t<playerPoints[i].GetStars.Length; t++)
			{
				playerStars[i, t] = playerPoints[i].GetStars[t].gameObject;
				playerStars[i, t].SetActive(false);
			}
		}
	}
	void OnGUI()
	{
		if(_isPaused)
		{
			GUI.Label(new Rect(Screen.width/2 - 55/2, Screen.height/3, 55, 50), text);
			if(GUI.Button(new Rect(Screen.width/2 - 100/2, Screen.height/2.0f, 100, 50), "Quit"))
			{
				Application.Quit();
			}
			if(GUI.Button(new Rect(Screen.width/2 - 100/2, Screen.height/2.5f, 100, 50), "Restart"))
			{
				Restart();
			}
		}
		if(connected[0])
			GUI.Label(new Rect(0, 0, 100, 25), PlayerPoints[0]);
		if(connected[1])
			GUI.Label(new Rect(Screen.width-100, 0, 100, 25), PlayerPoints[1]);
		if(connected[2])
			GUI.Label(new Rect(0, Screen.height-25, 100, 25), PlayerPoints[2]);
		if(connected[3])
			GUI.Label(new Rect(Screen.width-100, Screen.height-25, 100, 25), PlayerPoints[3]);
	}
	public void ConnectedPlayer(int i)
	{
		connected[i] = true;
	}

    public void DisconnectedPlayer(int i)
    {
        connected[i] = false;
    }

	public void TogglePaus()
	{
		_isPaused = !_isPaused;
		if(_isPaused)
			Time.timeScale = 0;
		else
			Time.timeScale = 1;
	}
	public void DisplayMessage(int p, int t)
	{
		points[p] = t;
		for(int i = 0; i < 4; i++)
		{
			if(connected[i])
			{
				for(int v = 0; v < 3; v++)
				{
					if(v < points[i])
						playerStars[i, v].SetActive(true);
					else
						playerStars[i, v].SetActive(false);
				}
			}
			else
			{
				for(int c = 0; c < 3; c++)
				{
					playerStars[i, c].SetActive(false);
				}
			}
		}
	}
	public void Restart()
	{
		Destroy(Camera.main.gameObject);
		Destroy(GameObject.Find("GameManager"));
		Destroy(playermanager);
		TogglePaus();
		Application.LoadLevel(0);
		Destroy(this.gameObject);
	}
}
