﻿using UnityEngine;
using System.Collections;

public class ProjectileLauncher : MonoBehaviour 
{
    [SerializeField]
    private Projectile[] m_projectilePrefab;

    public Projectile Fire(Vector2 velocity, float lifeTime, bool relativeToParent, int p)
    {
        Projectile proj = (Projectile)Instantiate(m_projectilePrefab[p], this.transform.position, Quaternion.identity);

        proj.LifeTime = lifeTime;
        proj.tag = tag;
        proj.Owner = this.gameObject;

        if (relativeToParent)
        {
            proj.Anchor = this.transform;
        }

        proj.SetVelocity(velocity);

        return proj;
    }

}
