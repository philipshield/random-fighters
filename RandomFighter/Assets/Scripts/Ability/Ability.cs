﻿using UnityEngine;
using System.Collections;

public class Ability : MonoBehaviour 
{
    [System.Flags]
    enum AbilityFlag
    {
        Jump    = (1 << 0),
        Attack = (1 << 1),
        Power = (1 << 2),
        Defensive = (1 << 3),
        Charge = (1 << 4)
    }
    
    [SerializeField]
    private AbilityFlag[] m_flags;

    [SerializeField]
    private XButton m_button;
    public XButton Button { get { return m_button; } set { m_button = value; } }

    public bool IsPressed { get; private set; }

    public XController Controller { get; private set; }
    public GroundSensor GroundSensor { get; private set; }
    public Pusher Pusher { get; private set; }
    public ProjectileLauncher ProjectileLauncher { get; private set; }
    public AnimController AnimController { get; private set; }
    public CharacterMove CharacterMove { get; private set; }
    public Knockout Knockout { get; private set; }

    public CharacterMove.CharacterDirection PlayerDirection { get { return CharacterMove.Direction; } }

    void Start()
    {
        Pusher = GetComponent<Pusher>();
        Controller = GetComponent<XController>();

        Controller.OnButtonPress += OnControllerButtonPressed;
        Controller.OnButtonRelease += OnControllerButtonReleased;

        GroundSensor = GetComponentInChildren<GroundSensor>();

        GroundSensor.OnHitGround += OnHitGround;
        GroundSensor.OnLeaveGround += OnLeaveGround;

        ProjectileLauncher = GetComponent<ProjectileLauncher>();
        AnimController = GetComponent<AnimController>();
        CharacterMove = GetComponent<CharacterMove>();
        Knockout = GetComponent<Knockout>();

    }

    void OnDestroy()
    {
        Controller.OnButtonPress -= OnControllerButtonPressed;
        Controller.OnButtonRelease -= OnControllerButtonReleased;

        GroundSensor.OnHitGround -= OnHitGround;
        GroundSensor.OnLeaveGround -= OnLeaveGround;
    }

    private void OnControllerButtonPressed(ButtonData data)
    {
        if (data.Button == Button && !Knockout.IsKnockedOut)
            OnPressed();
    }
    private void OnControllerButtonReleased(ButtonData data)
    {
        if (data.Button == Button && !Knockout.IsKnockedOut)
            OnReleased(data.HoldTime);
    }

    public virtual void OnPressed()
    {
        IsPressed = true;
    }

    public virtual void OnReleased(float holdTime)
    {
        IsPressed = false;    
    }

    public virtual void OnHitGround()
    {

    }

    public virtual void OnLeaveGround()
    {

    }
}
