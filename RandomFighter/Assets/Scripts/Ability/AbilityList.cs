﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AbilityList : MonoBehaviour 
{
    public GameObject[] list;
    public Ability jump;

    public List<Ability> GetRandomAbilities(int count)
    {
        //TODO: make sure flags are taken into account, ie jump at 0 etc

        List<Ability> abilities = new List<Ability>();

        foreach (GameObject o in list)
            abilities.Add(o.GetComponent<Ability>());


        List<Ability> result = new List<Ability>();

        result.Add(jump);

        for (int i = 1; i < count; ++i)
        {
            if (abilities.Count <= 0)
                break;

            int r = Random.Range(0, abilities.Count);
            result.Add(abilities[r]);
            abilities.RemoveAt(r);
        }

        return result;
    }
}
