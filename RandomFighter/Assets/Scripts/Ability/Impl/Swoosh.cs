﻿using UnityEngine;
using System.Collections;

public class Swoosh : Ability
{
    private float m_speed = 1;

    private float m_lifeTime = 0.5f;

    private float m_force = 30.0f;

    private float m_hitRadius = 1.9f;

    private float m_damage = 70.0f;

    [SerializeField, Tooltip("Set a positive X. It will be inverted when going left.")]
    private Vector2 m_forceDir = new Vector2(5, 1);

    private Projectile m_currentProjectile = null;

    private Vector2 m_dir;

    public override void OnPressed()
    {
        base.OnPressed();

        Punch();
    }

    void Update()
    {
        if (m_currentProjectile != null)
        {
            rigidbody2D.AddForce(-rigidbody2D.velocity * 10.0f + m_dir * 200.0f);
        }
    }

    void OnProjectileHitPlayer(GameObject player, float timeSinceLastHit, Projectile source)
    {
        if (timeSinceLastHit >= 0)
            return;

        player.GetComponent<Knockout>().DealDamage(m_damage);

        Vector2 impulse = m_forceDir.normalized;
        impulse *= m_force;

        if (source.rigidbody2D.velocity.x < 0)
            impulse.x *= -1.0f;

        player.GetComponent<Pusher>().ApplyImpulse(impulse);
    }

    void OnProjectileLifetimeExceeded(Projectile source)
    {
        m_currentProjectile.OnLifetimeExceeded -= OnProjectileLifetimeExceeded;
        m_currentProjectile.OnHitPlayer -= OnProjectileHitPlayer;

        m_currentProjectile = null;
        AnimController.StopActionAnimation();
    }

    void Punch()
    {
        if (m_currentProjectile == null)
        {
            float dir = PlayerDirection == CharacterMove.CharacterDirection.Left ? -1.0f : 1.0f;
            m_currentProjectile = ProjectileLauncher.Fire(Vector2.right * m_speed * dir, m_lifeTime, true, 0);
            m_currentProjectile.SetRadius(m_hitRadius);

            m_currentProjectile.OnLifetimeExceeded += OnProjectileLifetimeExceeded;
            m_currentProjectile.OnHitPlayer += OnProjectileHitPlayer;

            AnimController.StartActionAnimation("heavyPunch");
            AnimController.ActionAnimationPos = 0.5f;
            AnimController.ActionAnimationSpeed = 0.0f;

            m_dir = Controller.GetJoystickLeft().Force;
        }
    }
}
