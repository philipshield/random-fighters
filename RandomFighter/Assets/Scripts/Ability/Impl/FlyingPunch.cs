﻿using UnityEngine;
using System.Collections;

public class FlyingPunch : Ability
{
	[SerializeField]
	private float m_speed = 20;
	
	[SerializeField]
	private float m_lifeTime = 0.25f;
	
	[SerializeField]
	private float m_force = 30.0f;
	
	[SerializeField]
	private float m_hitRadius = 0.5f;
	
	[SerializeField]
	private float m_damage = 10.0f;
	
	[SerializeField]
	private float m_pullStrength = 10.0f;
	
	[SerializeField, Tooltip("Set a positive X. It will be inverted when going left.")]
	private Vector2 m_forceDir = new Vector2(5,5);
	
	private float m_cooldown = 3.0f;
	
	public GameObject Effect;
	
	void Update()
	{
		m_cooldown -= Time.deltaTime;
	}
	
	
	public override void OnPressed()
	{
		base.OnPressed();
		
		Punch();
	}
	
	void OnHitOther(GameObject other, float timeSinceLastHit, Projectile source)
	{
		Destroy(source.gameObject);
		CleanProjectile(source);
	}
	void OnHitProjectile(Projectile other, float timeSinceLastHit, Projectile source)
	{
		Destroy(source.gameObject);
		CleanProjectile(source);
	}
	
	void OnProjectileHitPlayer(GameObject player, float timeSinceLastHit, Projectile source)
	{
		if (timeSinceLastHit >= 0)
			return;
		if (source != null)
		{
			player.GetComponent<Knockout>().DealDamage(m_damage);
			
			Vector2 impulse = m_forceDir.normalized;
			impulse *= m_force;
			
			if (source.rigidbody2D.velocity.x < 0)
				impulse.x *= -1.0f;
			
			player.GetComponent<Pusher>().ApplyImpulse(impulse);
			
			
		}
	}
	
	void OnProjectileLifetimeExceeded(Projectile source)
	{
		CleanProjectile(source);
	}
	
	void Punch()
	{
		if (m_cooldown <= 0.0f)
		{
		
			float dir = PlayerDirection == CharacterMove.CharacterDirection.Left ? -1.0f : 1.0f;
			Projectile m_currentProjectile = ProjectileLauncher.Fire(Vector2.right * m_speed * dir + rigidbody2D.velocity * 0.7f, m_lifeTime, false, 1);
			m_currentProjectile.SetRadius(m_hitRadius);
			m_currentProjectile.Effect = Effect;
			
			m_currentProjectile.OnLifetimeExceeded += OnProjectileLifetimeExceeded;
			m_currentProjectile.OnHitPlayer += OnProjectileHitPlayer;
			m_currentProjectile.OnHitOther += OnHitOther;
			m_currentProjectile.OnHitProjectile += OnHitProjectile;
			
			AnimController.StartActionAnimation("heavyPunch");
			AnimController.ActionAnimationSpeed = 2.0f;
			
			m_currentProjectile.gameObject.AddComponent("HingeJoint2D");
			m_currentProjectile.GetComponent<HingeJoint2D>().connectedBody = ProjectileLauncher.transform.gameObject.GetComponent<Rigidbody2D>();
			
			m_cooldown = 1.0f;
		}
	}
	
	void CleanProjectile(Projectile proj)
	{
		proj.OnLifetimeExceeded -= OnProjectileLifetimeExceeded;
		proj.OnHitPlayer -= OnProjectileHitPlayer;
		proj.OnHitOther -= OnHitOther;
		proj.OnHitProjectile -= OnHitProjectile;
	}
}
