﻿using UnityEngine;
using System.Collections;

public class BasicJump : Ability 
{
    [SerializeField]
    private int m_numberOfAirjumps = 1;

    [SerializeField]
    private float m_jumpForce = 20.0f;

    private int m_extraJumpCount = 1;

    public override void OnPressed()
    {
        base.OnPressed();

        if (GroundSensor.IsOnGround)
        {
            Jump();
        }
        else if (m_extraJumpCount > 0)
        {
            Jump();
            --m_extraJumpCount;
        }
    }

    public override void OnHitGround()
    {
        base.OnHitGround();

        m_extraJumpCount = m_numberOfAirjumps;
    }

    private void Jump()
    {
        Vector2 vel = rigidbody2D.velocity;
        vel.y = m_jumpForce;

        Pusher.SetVelocity(vel);
		AnimController.StartActionAnimation("jump");
    }
}
